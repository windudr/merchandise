#include <iostream>
using namespace std;

struct Produk{
    int nomor;
    char nama;
    int harga;
    Produk* next;
    Produk* prev;
};
typedef Produk *pointerProduk;
typedef pointerProduk ListProduk;

void createListProduk(ListProduk &head){
    head=NULL;
}

void createProduk(pointerProduk &newProduk){
    newProduk=new Produk;
    cout << "Nomor produk: ";cin >> newProduk->nomor; //masukkin data produk
    cout << "Nama produk: ";cin >> newProduk->nama;
    cin.ignore();
    cout << "Harga produk: ";cin >> newProduk->harga;
    newProduk->next=NULL;
    newProduk->prev=NULL;
}

void insertProduk(ListProduk &head,pointerProduk newProduk){
    if(head==NULL){
        head=newProduk;
    }
    else{
        newProduk->next=head;
        head->prev=newProduk;
        head=newProduk;
    }
}

void searchProduk(ListProduk head, pointerProduk &cariProduk, int nomorCari){
    cout << "Masukkan nomor produk : ";cin >> nomorCari;
    cariProduk=head;
    while(cariProduk!=NULL){          //mencari...
        if(cariProduk->nomor==nomorCari){    //kalau ketemu
            break;
        }
        else{
            cariProduk=cariProduk->next;    //kalau belum ketemu
        }
    }
}

//Kalo mau delete jangan semuanya dulu, bisa eror
void deleteProduk(ListProduk &head, pointerProduk &deleteProduk, pointerProduk cariProduk){
    if(head==NULL || cariProduk==NULL){   //kalau tidak ketemu
        cout << "Data Null!" << endl;
    }
    else if(cariProduk==head){                        //kalau di head
        deleteProduk=head;
        head=head->next;
        head->prev=NULL;
        deleteProduk->next=NULL;
        deleteProduk->prev=NULL;
    }
    else{                                           //kalau selain di head
        deleteProduk=cariProduk;
        cariProduk->prev->next=cariProduk->next;
        cariProduk->next->prev=cariProduk->prev;
        deleteProduk->next=NULL;
        deleteProduk->prev=NULL;
    }
    
}

void traversal(ListProduk head){
    pointerProduk tmp;
    if(head==NULL){
        cout << "Kosong" << endl;
    }
    else{
        tmp=head;
        while(tmp!=NULL){
            cout << "Nomor:" << tmp->nomor << endl;
            cout << "Nama:" << tmp->nama << endl;
            cout << "Harga:" << tmp->harga << endl;
            tmp=tmp->next;
        }
    }
}

//ceritanya pake bubble sort untuk sorting
//bisa dimodif buat sortir sesuai harga atau tipe data angka
void sortProdukAsc(ListProduk &head){   
    int swapped, i;
    pointerProduk temp;
    pointerProduk lptr = NULL;

    if (head==NULL){
        return;
    }

    do{
        swapped=0;
        temp = head;

        while (temp->next != lptr){
            if (temp->nomor > temp->next->nomor){
                swap(temp->nomor, temp->next->nomor);
                swap(temp->harga, temp->next->harga);
                swap(temp->nama, temp->next->nama);
                swapped=1;
            }
            temp=temp->next;
        }
        lptr = temp;
    }while(swapped);
}
//buat sorting descending, sama aja kayak yang di atas
//bisa dimodif buat sortir sesuai harga atau tipe data angka
void sortProdukDesc(ListProduk &head){
    int swapped, i;
    pointerProduk temp;
    pointerProduk lptr = NULL;

    if (head==NULL){
        return;
    }

    do{
        swapped=0;
        temp = head;

        while (temp->next != lptr){
            if (temp->nomor < temp->next->nomor){
                swap(temp->nomor, temp->next->nomor);
                swap(temp->harga, temp->next->harga);
                swap(temp->nama, temp->next->nama);
                swapped=1;
            }
            temp=temp->next;
        }
        lptr = temp;
    }while(swapped);
}

main(){
    pointerProduk newProduk, delProduk, cariProduk;
    ListProduk listProduk;
    int nomorCari, pilih;
    createListProduk(listProduk);
    do{
        system("cls");
        traversal(listProduk);
        cout << "1. Masukkan produk" << endl;
        cout << "2. Sortir produk ascending" << endl;
        cout << "3. Sortir produk descending" << endl;
        cout << "4. Hapus produk" << endl;
        cout << "5. Keluar" << endl;
        cout << "Pilihan anda: ";cin >> pilih;
        switch(pilih){
            case 1:
                createProduk(newProduk);
                insertProduk(listProduk, newProduk);
                break;
            case 2:
                sortProdukAsc(listProduk);
                break;
            case 3:
                sortProdukDesc(listProduk);
                break;
            case 4:
                searchProduk(listProduk, cariProduk, nomorCari);
                deleteProduk(listProduk, delProduk, cariProduk);
                break;
            case 5:
                break;
        }
    }while(pilih!=5);
}